#include <LiquidCrystal.h>
#include "HX711.h"

const int BOTAO_ESQUERDO = 6;
const int BOTAO_DIREITO = 7;
const int MOTOR_PIN8 = 8;
const int MOTOR_PIN11 = 11;

const int QUANTIDADE_COLUNAS_LCD = 20;
const int QUANTIDADE_LINHAS_LCD = 2;

const int LOADCELL_DOUT_PIN = 9;
const int LOADCELL_SCK_PIN = 10;

// MENU
int opcao = 0;
const int MENU = 0;
const int CALIBRAR = 1;
const int TARA = 2;
const int INICIAR = 3;
const int SAIR = 4;

const int TEMPO_EVENTO = 150;

HX711 balanca;
LiquidCrystal lcd(0, 1, 2, 3, 4, 5);

void setup() {
  lcd.begin(QUANTIDADE_COLUNAS_LCD, QUANTIDADE_LINHAS_LCD);
  
  pinMode(BOTAO_ESQUERDO, INPUT_PULLUP);
  pinMode(BOTAO_DIREITO, INPUT_PULLUP);

  pinMode(MOTOR_PIN8, OUTPUT);
  pinMode(MOTOR_PIN11, OUTPUT);

  balanca.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  zerarBalanca();
}

void loop() {
    delay(TEMPO_EVENTO);
    
   if(opcao == MENU){
    escreverLcd("MENU");
   }else if(opcao == CALIBRAR){
    escreverLcd("CALIBRAR");
    if(digitalRead(BOTAO_DIREITO) == LOW){
      calibrar();  
    }
    }else if(opcao == TARA){
     escreverLcd("TARA");
     if(digitalRead(BOTAO_DIREITO) == LOW){
      tara();
    }
   }else if(opcao == INICIAR){
    escreverLcd("INICIAR");
    if(digitalRead(BOTAO_DIREITO) == LOW){
      pesar();
    }
   }else if(opcao == SAIR){
    escreverLcd("SAIR");
   }

   if(digitalRead(BOTAO_ESQUERDO) == LOW){
      if(opcao > SAIR){
        opcao = MENU;
      }else{
        opcao ++;
      }
   }  
}

void calibrar(){
  delay(TEMPO_EVENTO);
  float pesoKG = 0.00;
  long pesoG = 0;
  bool calibrado = false;
  escreverLcd("PESO " + String(pesoG) + " g");
  while(!calibrado){
     delay(TEMPO_EVENTO);
    if(digitalRead(BOTAO_DIREITO) == LOW){
      calibrado = true;
      long calibration_factor = balanca.get_units(5) / pesoKG;
      balanca.set_scale(calibration_factor);
      escreverLcd("CALIBRANDO");
      delay(TEMPO_EVENTO);
    }else if(digitalRead(BOTAO_ESQUERDO) == LOW){
      pesoKG += 0.10;
      pesoG = pesoKG * 1000;
      escreverLcd("PESO " + String(pesoG) + " g");
    }
  }
}

void pesar(){
  long pesoSelecionado = 100;
  delay(TEMPO_EVENTO);
  bool pesando = true;
  while(pesando){
    if(digitalRead(BOTAO_DIREITO) == LOW){
      pesando = false;
    } else {
      long peso = balanca.get_units() * 1000;
      if(peso >= pesoSelecionado){
        digitalWrite(MOTOR_PIN8, HIGH);
        digitalWrite(MOTOR_PIN11, LOW);
    }else{
      digitalWrite(MOTOR_PIN8, LOW);
      digitalWrite(MOTOR_PIN11, LOW);
    }
      escreverLcd("PESO: " + String(peso) + " g");
    }
  }
}

void tara(){
   escreverLcd("TARANDO");
   delay(TEMPO_EVENTO);
   balanca.tare();
}

void escreverLcd(String mensagem){
  lcd.clear();
  lcd.print(mensagem);
}

void zerarBalanca(){
  balanca.set_scale();
  delay(TEMPO_EVENTO);
  escreverLcd("INICIANDO BALANCA");
  balanca.tare();
}
